package Examen1;
import java.io.*;

public class Ejercicio3 {
	static String[] datos;

	
	public static void leeFichero(File fichero){
		
		try {
			FileReader fr = new FileReader(fichero);
			int letra=0, posicion=0;
			String cadena="";
			
			letra = fr.read();
			while(letra!=-1)
			{
				if(letra!='-')
				{
					cadena+= (char)letra;
				}else
				{
					datos[posicion]=cadena;
					cadena="";
					posicion++;
				}
			letra = fr.read();
			}
			datos[0]=(datos.length-1)/2+"";
			fr.close();
			
		} catch (FileNotFoundException e) {
			System.out.println(e.getMessage());		
		} catch (IOException e) {
			System.out.println(e.getMessage());
		}
		
	}
	public static void escribirDatos(File fichero){
		for(int i =1; i<datos.length-1; i++)
		{
			System.out.println(datos[i]);
		}
	}
	
	public static void dameDNIMenor() {
		int aux, parse=0;
		String nombre="";
		
		aux= Integer.parseInt(datos[2]);
		for(int i=2; i < datos.length; i+=2)
		{
			parse =Integer.parseInt(datos[i]);
			if(parse <= aux)
			{
				nombre= datos[i-1];
				aux=parse;
			}
		
		}System.out.println("El DNI menor es"+nombre);
	}
	
	public static void dameDNIMayor() {
		int aux=-1, parse=0;
		String nombre="";
		
		for(int i=2; i < datos.length; i+=2)
		{
			parse =Integer.parseInt(datos[i]);
			if(parse > aux)
			{
				nombre= datos[i-1];
				aux=parse;
			}
		}System.out.println("El DNI Mayor es"+nombre);
	}
	public static void main(String[] args) {
		File fichero= new File("JavaFicheros/Examen1/Empleados.txt");
		datos = new String [10];
		leeFichero(fichero);
		escribirDatos(fichero);
		dameDNIMayor();
	    dameDNIMenor();
		

	}

}
