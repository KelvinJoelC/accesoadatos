package Examen1;

import java.io.*;

public class Ejercicio3Correcion {
	
	public static void leerFichero(String nombre, String[] tNombres, int[] tdni) {
		
		int resultado;		//variable para leer
		int i;				//Indice tablas
		String valorCadena;
		boolean numero, primero;
		
		
		
		try {
			File fichero = new File("JavaFicheros/Examen1/Empleados.txt");
			FileReader fr = new FileReader(fichero);
			
			numero= true;
			primero= true;
			i=0;
			valorCadena="";
			
			
			resultado = fr.read();
			while(resultado!=-1)
			{
				if(resultado!='-')
				{
					valorCadena+= (char)resultado;
				}else
				{
					if(numero==true)
					{
						if(primero==true)
						{
							primero=false;
						}else
						{
							tdni[i]=Integer.parseInt(valorCadena);
							i++;
							
						}
						numero=false;
					}else
					{
						tNombres[i]=valorCadena;
						numero=true;
					}
					valorCadena="";
				}
				resultado = fr.read();
			}
			fr.close();
			
			
			
		} catch (IOException e) {
			System.out.println(e.getMessage());
		}
	}
	
	public static void escribirDatos(String[] tNombres, int[] tDNIs)
	{
		System.out.println("El contenido es: ");
		for(int i=0; i<tNombres.length;i++)
		{
			System.out.println(tNombres[i]+" - "+tDNIs[i]);
		}
	}
	
	public static void escribirMayorDNI(String[] tNombres, int[] tDNIs) {
		int DNIMaximo, posMax;
		
		DNIMaximo=-1;
		posMax=0;
		for(int i=0; i<tDNIs.length;i++)
		{
			if(tDNIs[i] > DNIMaximo)
			{
				DNIMaximo = tDNIs[i];
				posMax =i;
			}
		
		}
		System.out.println("El nombre del DNI mayor es " + tNombres[posMax]);
	}
	
	public static void escribirMenorDNI(String[] tNombres, int[] tDNIs) {
		int DNIMinimo, posMin;
		
		DNIMinimo=1000000;
		posMin=0;
		for(int i=0; i<tDNIs.length;i++)
		{
			if((tDNIs[i]!=0)&&(tDNIs[i] < DNIMinimo))
			{
				DNIMinimo = tDNIs[i];
				posMin =i;
			}
		
		}
		System.out.println("El nombre del DNI mayor es " + tNombres[posMin]);
	}
	

	public static void main(String[] args) {
		String[] tablaNombre= new String[10];
		int[] tablaDNIs = new int[10];
		
		leerFichero("Empleados.txt", tablaNombre, tablaDNIs);
		escribirDatos(tablaNombre, tablaDNIs);
		escribirMayorDNI(tablaNombre, tablaDNIs);
		escribirMenorDNI(tablaNombre, tablaDNIs);
		

	}

}
