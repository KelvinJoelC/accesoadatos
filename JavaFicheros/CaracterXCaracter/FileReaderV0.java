package CaracterXCaracter;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;

public class FileReaderV0 {
	
	//LECTURA AVANZADA.
	public static void LeerFichero(String nombre)
	{ 
		try
		{
		//Apertura del fichero
		File fichero= new File(nombre);
		FileReader fr = new FileReader(fichero);
		
		//Leectura del fichero.
		int letra;
		String cadena;
		
		cadena="";
		letra = fr.read();
		while (letra!=-1)
		{
			//tratamiento del car�cter leido
			cadena += ((char)letra);
			//Leer el siguiente caracter
			letra = fr.read();
		}
		//Cerrar el fichero.
		fr.close();
		
		//Imprimir la cadena por pantalla
		System.out.println(cadena);

		} catch (IOException e) {
			System.out.println(e.getMessage());
		}
		
		
	}

	public static void main(String[] args) {
		LeerFichero("JavaFicheros/Pruebas/FicheroTxt1.txt");
	}

}
