package CaracterXCaracter;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

public class FileWriterV0 {
	
	public static void escribirFichero() {
	try
	{	
		//Apertura del fichero
		File fichero= new File("JavaFicheros/Pruebas/FicheroTxt1.txt");
		FileWriter fw= new FileWriter(fichero);
		
		// Escribir en el fichero
		fw.write("Juan");
		fw.write("Ana");
		fw.write("Pepe");
		
		
		//Cerrar Fichero
		fw.close();
		
		
	} catch (IOException e) 
	{
		System.out.println(e.getMessage());

	}
	}
	public static void main(String[] args) {
		escribirFichero();
	}

}
