package CreandoCarpetas;

import java.io.File;
import java.io.IOException;

public class creandoCarpetasV0 {

	public static void main(String[] args) {
		File carpeta = new File("JavaFicheros/CreandoCarpetas/Ficheros");
		if(carpeta.mkdir()||carpeta.exists())
		{
			System.out.println("Carpeta creada");
			File fichero1= new File(carpeta, "primero.txt");
			File fichero2= new File(carpeta, "segundo.txt");

		try 
		{
			fichero1.createNewFile();
			fichero2.createNewFile();
		} catch (IOException e) {
			System.out.println(e.getMessage()); //Coge el mesaje de error y lo saca por pantalla
		}
		}else 
			System.out.println("No se ha podido crear la carpeta");

	}

}
