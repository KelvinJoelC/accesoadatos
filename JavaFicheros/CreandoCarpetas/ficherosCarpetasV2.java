package CreandoCarpetas;

import java.io.File;

public class ficherosCarpetasV2 {

	public static void main(String[] args) {
		String[] rutas = {
				"Prueba",
				"Prueba/bin",
				"Prueba/bin/bytecode",
				"Prueba/bin/objetos",
				"Prueba/src",
				"Prueba/src/clases",
				"Prueba/src/objetos",
				"Prueba/doc",
				"Prueba/doc/html",
				"Prueba/doc/pdf"};
				
		boolean seguir= true;
		
		
		
		for(int i=0;(i<rutas.length) && (seguir==true);i++)
		{	
			File carpeta = new File(rutas[i]);
			if((seguir = carpeta.mkdir())==false) {
				System.out.println("No se ha podido crear "+rutas[i]);
				seguir=false;	
			}
		}
		//Cuando un archivo no se pueda crear se cancele todo lo siguiente

	}

}
