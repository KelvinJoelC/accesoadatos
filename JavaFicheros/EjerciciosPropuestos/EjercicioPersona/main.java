package EjerciciosPropuestos.EjercicioPersona;
import java.io.*;

public class main {

	public static void main(String[] args) {
		File ruta= new File("JavaFicheros/EjerciciosPropuestos/EjercicioPersona/fichero");
		Persona[] arrayPersona=new Persona[5];
		
		String[] Nombre= {"Kelvin","Jorge","Humberto","Diego","Javier"};
		String[] Apellido = {"Carre�o","Loarte","Valverde","Mompo","Elvira"};
 		int[] Edad = {20,21,22,23,24};
 		
		for(int i=0;i<arrayPersona.length;i++) {
			arrayPersona[i]= new Persona(Nombre[i],Apellido[i],Edad[i]); 
		}
 		
 		
		leeFichero(arrayPersona,ruta);
		escribirFichero(arrayPersona,ruta);
		imprimir(arrayPersona,ruta);
		vaciar(arrayPersona);
 		imprimir(arrayPersona,ruta);
 		
	}

	public static void escribirFichero(Persona[] arrayPersona ,File ruta) {
		

		try {
			
			FileWriter fw = new FileWriter(ruta);
			
			for(int i=0;i<arrayPersona.length;i++) {
			fw.write("/"+arrayPersona[i].getNombre()+"-"+arrayPersona[i].getApellido()+";"+arrayPersona[i].getEdad()+"$\n");
			}
			fw.close();
			
		} catch (FileNotFoundException e) {
			
			System.out.println(e);
		} catch (IOException e) {

			System.out.println(e);	
		}
		
		
		System.out.println("creado");
	}
	
	public static void leeFichero(Persona[] arrayPersona ,File ruta) {
		String Nombre="";
		String Apellido="";
		String Edad="";
		String txt ="";
		int posicion=0;
		try {			
			FileReader frUsuarios = new FileReader(ruta);
			BufferedReader bf = new BufferedReader(frUsuarios);

			while ((txt = bf.readLine()) != null) {

				Nombre = txt.substring(txt.indexOf('/') + 1, txt.indexOf('-'));
				Apellido = txt.substring(txt.indexOf('-') + 1, txt.indexOf(';'));
				Edad = txt.substring(txt.indexOf(';') + 1, txt.indexOf('$'));
				
				arrayPersona[posicion]= new Persona(Nombre, Apellido, Integer.parseInt(Edad));
				posicion++;				
				}
			
			bf.close();

		} catch (IOException e) {
			System.out.println("Error al cargar el fichero usuario");
		}
	}
	public static void imprimir(Persona[] arrayPersona ,File ruta) {
		for(int i=0;i<arrayPersona.length;i++) {
		System.out.println(arrayPersona[i].getNombre()+" "+arrayPersona[i].getApellido()+" "+arrayPersona[i].getEdad());
		}
	}
	
	public static void vaciar(Persona[] arrayPersona) {
		
		for(int i=0;i<arrayPersona.length;i++) {
		arrayPersona[i]=new Persona();
		}
	}

}

