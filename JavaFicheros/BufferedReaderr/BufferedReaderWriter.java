package BufferedReaderr;

public class BufferedReaderWriter {

	public static void main(String[] args) {
		String [] miTabla = {"Juan","Laura","Mario","Rodrigo"};
		String[] tablaVacia = new String[10];
		FicheroEnBuffer miFichero= new FicheroEnBuffer("JavaFicheros/Pruebas/Nombre.txt");
		miFichero.escribirTabla(miTabla);
		miFichero.leerTabla(tablaVacia);
		for(int i=0; i<tablaVacia.length; i++) {
			System.out.println(tablaVacia[i]);			
		}
		
		Persona[] misPersonas= new Persona[3];
		misPersonas[0]= new Persona("Juan",19);
		misPersonas[1]= new Persona("Isabel",20);
		misPersonas[2]= new Persona("Jorge",25);
	}
	
	
}
