package BufferedReaderr;

import java.io.*;

public class FicheroEnBuffer {

	private String nombreFichero;
	
	public FicheroEnBuffer(String nombre)
	{
		nombreFichero=nombre;
	}
	
	public void escribirTabla(String[] tabla) {
		
		try {
			File f = new File(nombreFichero);
			FileWriter fw = new FileWriter(f);
			BufferedWriter bw = new BufferedWriter(fw);
			
			for(int i=0; i<tabla.length;i++) {
			bw.write(tabla[i]);
			bw.newLine();
			}
			
			bw.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
public void leerTabla(String[] tabla) {
		
		try {
			File f = new File(nombreFichero);
			FileReader fw = new FileReader(f);
			BufferedReader br = new BufferedReader(fw);
			String linea;
			int i =0;
			
			linea = br.readLine();
			while(linea !=null) {
				tabla[i]=linea;
				i++;
				linea = br.readLine();
			}
			br.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
}
